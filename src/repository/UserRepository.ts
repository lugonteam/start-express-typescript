import {Service} from "typedi";
import {getMongoRepository} from "typeorm";

@Service()
export class UserRepository {
    async getUsers(limit:number, page:number, usersPerPage:number, order:string) {
        const skip = (limit < 1 ? 0 : page - 1) * usersPerPage;
        const sort = order == "asc" ? 1 : -1;
        // const users = await getMongoRepository(User).aggregate([
        //     {
        //         $lookup: {
        //             from: 'user-ref',
        //             localField: '_id',
        //             foreignField: 'user',
        //             as: 'ref'
        //         }
        //     }, {
        //         $unwind: "$ref"
        //     }, {
        //         $lookup: {
        //             from: 'user-info',
        //             localField: '_id',
        //             foreignField: 'user',
        //             as: 'info'
        //         }
        //     }, {
        //         $unwind: "$info"
        //     }, {
        //         $addFields: {user: "$$ROOT"}
        //     }, {
        //         $sort: {_id: sort}
        //     }, {
        //         $facet: {
        //             paginatedResults: [{$skip: skip}, {$limit: limit}],
        //             totalCount: [{
        //                 $count: 'count'
        //             }]
        //         }
        //     }, {
        //         $unwind: "$paginatedResults"
        //     }, {
        //         $unwind: "$totalCount"
        //     }
        // ]);
        //
        // let totalCount = 0;
        // const usersArray = (await users.toArray()).map(({paginatedResults, totalCount: {count}}) => {
        //     totalCount = count;
        //     const {user, info, ref} = paginatedResults;
        //     return {user, info, ref};
        // });
        //
        // return {
        //     pagination: {totalCount, page: limit, perPage: usersPerPage},
        //     data: usersArray
        // };
    }
}
