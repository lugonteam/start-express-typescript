import {join} from 'path';
import {$log} from "ts-log-debug";

export function getOsEnv(key: string, defaultValue: string = null): string {
    if (typeof process.env[key] === 'undefined' && !process.env[key]) {
        if (!defaultValue) $log.error(`env ${key} is not set.`);
        return defaultValue || null;
    }

    return process.env[key] as string;
}

export function getOsEnvOptional(key: string): string | undefined {
    return process.env[key];
}

export function getPath(path: string): string {
    return (process.env.NODE_ENV && process.env.NODE_ENV.startsWith("prod"))
        ? join(process.cwd(), path.replace('src/', 'dist/').replace(".ts", ".js"))
        : join(process.cwd(), path);
}

export function getPaths(paths: string[]): string[] {
    return paths.map(p => getPath(p));
}

export function getOsPath(key: string, defaultValue: string = null): string {
    return getPath(getOsEnv(key, defaultValue));
}

export function getOsPaths(key: string, defaultValue: string = null): string[] {
    return getPaths(getOsEnvArray(key, defaultValue));
}

export function getOsEnvArray(key: string, defaultValue: string = null, delimiter: string = ','): string[] {
    if (!process.env) return [];
    return getOsEnv(key, defaultValue)?.split(delimiter) || [];
}

export function toNumber(value?: string): number {
    if (!value) return 0;
    return parseInt(value, 10);
}

export function toBool(value?: string): boolean {
    if (!value) return false;
    return value === 'true';
}

export function normalizePort(port?: string): number | string | boolean {
    if (!port) return false;
    const parsedPort = parseInt(port, 10);
    if (isNaN(parsedPort)) { // named pipe
        return port;
    }
    if (parsedPort >= 0) { // port number
        return parsedPort;
    }
    return false;
}