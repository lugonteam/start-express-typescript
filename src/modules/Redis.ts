import {Service} from "typedi";
import IORedis from "ioredis";

@Service()
export class Redis {

    static MINUTE_59s = 58
    static MINUTE_1 = 60
    static MINUTE_1_5 = 90
    static MINUTE_2 = 120
    static MINUTE_5 = 60 * 5
    static MINUTE_10 = 60 * 10
    static MINUTE_15 = 60 * 15
    static MINUTE_30 = 60 * 30
    static HOUR_1 = 60 * 60
    static HOUR_12 = 60 * 60 * 12
    static DAY_1 = 60 * 60 * 24
    static WEEK_1 = 60 * 60 * 24 * 7
    static WEEK_2 = 60 * 60 * 24 * 7 * 2
    static WEEK_3 = 60 * 60 * 24 * 7 * 3
    static MONTH_1 = 60 * 60 * 24 * 7 * 4

    private readonly ioRedis

    constructor() {
        this.ioRedis = new IORedis({
            host: "localhost",
            port: 6379
        })
    }

    setCache = async (key, value, expire?: number) => {
        return this.ioRedis.set(key, JSON.stringify(value), 'EX', expire | 30)
            .then(() => {
                console.log(`REDIS: key ${key} set cache!`)
                return value
            })
            .catch(err => {
                console.log('ERROR_REDIS: Timeout exceeded', err)
                return value
            });
    }

    getCache = async (key) => {
        return this.ioRedis.get(key)
            .then(cache => cache ? JSON.parse(cache) : null)
            .catch(_ => null);
    }
}
