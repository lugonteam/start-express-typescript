import {useContainer as classValidatorUseContainer} from 'class-validator';
import {useContainer as routingUseContainer, ILoader} from 'express-lugon';
import {Container} from 'typedi';
import {useContainer as ormUseContainer} from 'typeorm';
import {$log} from "ts-log-debug";
import {fileNameByPath} from "../utils";

export const iocLoader: ILoader | any = () => {
    $log.info("Loader: " + fileNameByPath(__filename));
    /**
     * Setup routing-controllers to use typedi container.
     */
    routingUseContainer(Container);
    ormUseContainer(Container);
    classValidatorUseContainer(Container);
};
