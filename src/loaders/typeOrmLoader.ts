import {createConnection, getConnectionOptions, useContainer} from 'typeorm';
import {env} from '../env';
import {ILoader, ServerLoader} from "express-lugon";
import {Container} from "typedi";
import {$log} from "ts-log-debug";
import {fileNameByPath} from "../utils";

export const typeOrmLoader: ILoader | any = async (_: ServerLoader) => {
    $log.info("Loader: " + fileNameByPath(__filename));

    const loadedConnectionOptions = await getConnectionOptions();

    useContainer(Container);
    const mongoConfig = env.db.url ? {
        url: `mongodb+srv://${env.db.username}:${env.db.password}@${env.db.url}/${env.db.database}?retryWrites=true&w=majority`,
    } : {
        host: env.db.host,
        port: env.db.port,
        username: env.db.username,
        password: env.db.password,
        database: env.db.database,
    };
    const mongoClientOptions = Object.assign({
        type: env.db.type as any, // See createConnection options for valid types
        synchronize: env.db.synchronize,
        logging: env.db.logging,
        entities: env.app.dirs.entities,
        useUnifiedTopology: true,
        useNewUrlParser: true,
    }, mongoConfig);

    const connectionOptions = Object.assign(loadedConnectionOptions, mongoClientOptions);

    await createConnection(connectionOptions).then(_ => {
        $log.info("Logged DB")
    }).catch((e) => {
        console.log("error: createConnection");
        console.log(e)
    });
};
