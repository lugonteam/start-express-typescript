import express from 'express';
import path from 'path';
import {ILoader, ServerLoader} from "express-lugon";
import {$log} from "ts-log-debug";
import favicon from 'serve-favicon';
import {fileNameByPath} from "../utils";

export const publicLoader: ILoader | any = (app: ServerLoader) => {
    $log.info("Loader: " + fileNameByPath(__filename));
    if (app) {
        const {expressApp} = app;
        expressApp
            // Serve static files like images from the public folder
            .use(express.static(path.join(__dirname, '..', 'public'), {maxAge: 31557600000}));

            // A favicon is a visual cue that client software, like browsers, use to identify a site
            // .use(favicon(path.join(__dirname, '../../..', 'src/frontend/public', 'favicon.ico')));

    }
};
