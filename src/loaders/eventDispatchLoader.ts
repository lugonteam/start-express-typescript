import glob from 'glob';

import {env} from '../env';
import {ILoader, ServerLoader} from "express-lugon";
import {$log} from "ts-log-debug";
import {fileNameByPath} from "../utils";

/**
 * eventDispatchLoader
 * ------------------------------
 * This loads all the created subscribers into the project, so we do not have to
 * import them manually
 */
export const eventDispatchLoader: ILoader | any = (app: ServerLoader) => {
    $log.info("Loader: " + fileNameByPath(__filename));
    if (app) {
        const patterns = env.app.dirs.subscribers;
        patterns.forEach((pattern) => {
            glob(pattern, (err: any, files: string[]) => {
                for (const file of files) {
                    require(file);
                }
            });
        });
    }
};
