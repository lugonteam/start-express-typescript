import {configure, format, transports} from 'winston';

import {env} from '../env';
import {ILoader} from "express-lugon";
import {$log} from "ts-log-debug";
import {fileNameByPath} from "../utils";

export const winstonLoader: ILoader = () => {
    $log.info("Loader: " + fileNameByPath(__filename));
    configure({
        transports: [
            new transports.Console({
                level: env.log.level,
                handleExceptions: true,
                format: env.node !== 'development'
                    ? format.combine(
                        format.json()
                    )
                    : format.combine(
                        format.colorize(),
                        format.simple()
                    ),
            }),
        ],
    });
};
