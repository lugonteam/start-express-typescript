import express from 'express';

import {env} from '../env';
import {ILoader, ServerLoader} from "express-lugon";
import {$log} from "ts-log-debug";
import {fileNameByPath} from "../utils";

export const homeLoader: ILoader | any = (app: ServerLoader) => {
    $log.info("Loader: " + fileNameByPath(__filename));
    if (app) {
        const {expressApp} = app;
        expressApp.get(
            env.app.routePrefix,
            (req: express.Request, res: express.Response) => {
                return res.json({
                    name: env.app.name,
                    version: env.app.version,
                    description: env.app.description,
                    id: env.app.port,
                });
            }
        );
    }
};
