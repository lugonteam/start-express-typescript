import compression from 'compression';
import express from 'express';
import { ExpressMiddlewareInterface, Middleware } from "express-lugon";

@Middleware({ type: 'before' })
export class CompressionMiddleware implements ExpressMiddlewareInterface {

    public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
        return compression()(req, res, next);
    }

}
