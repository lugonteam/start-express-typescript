import express from 'express';
import {ExpressMiddlewareInterface, Middleware} from "express-lugon";

@Middleware({type: 'before'})
export class NetworkMiddleware implements ExpressMiddlewareInterface {

    public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
        const {network} = req.query
        req.query["network"] = network === "bsc" || network === "matic" ? network : 'eth'

        next();
    }

}
