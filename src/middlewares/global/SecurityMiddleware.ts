import express from 'express';
import helmet from 'helmet';
import { ExpressMiddlewareInterface, Middleware } from "express-lugon";

@Middleware({ type: 'before' })
export class SecurityMiddleware implements ExpressMiddlewareInterface {

    public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
        return helmet()(req, res, next);
    }

}
