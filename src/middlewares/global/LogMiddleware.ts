import express from 'express';
import morgan from 'morgan';
import { ExpressMiddlewareInterface, Middleware } from "express-lugon";
import {env} from "../../env";
import {$log} from "ts-log-debug";

@Middleware({ type: 'before' })
export class LogMiddleware implements ExpressMiddlewareInterface {

    public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
        return morgan(env.log.output, {
            stream: {
                write: $log.info.bind($log),
            },
        })(req, res, next);
    }

}
