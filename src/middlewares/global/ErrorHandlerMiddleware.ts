import express from 'express';
import {ExpressErrorMiddlewareInterface, HttpError, Middleware} from "express-lugon";
import {env} from "../../env";
import {$log} from "ts-log-debug";

@Middleware({type: 'after'})
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {

    public isProduction = env.isProduction;

    public error(error: HttpError, req: express.Request, res: express.Response, next: express.NextFunction): void {
        res.status(error.httpCode || 500);
        res.json({
            name: error.name,
            message: error.message,
            errors: error[`errors`] || [],
        });

        if (this.isProduction) {
            $log.error(error.name, error.message);
        } else {
            $log.error(error.name, error.stack);
        }

    }

}
