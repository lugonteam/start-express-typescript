import express from 'express';
import {ExpressMiddlewareInterface, Middleware} from "express-lugon";
import {verify} from "jsonwebtoken";
import {AccessTokenDetection} from "../../auth/authorizationChecker";
import {ACCESS_TOKEN_SECRET} from "../../auth/config";

@Middleware({type: 'before'})
export class AccessTokenMiddleware implements ExpressMiddlewareInterface {

    public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
        const accessTokenDetection = AccessTokenDetection(req);
        const accessToken = req.cookies["access-token"] || req.header('x-access-token') || accessTokenDetection;
        if (accessToken) {
            try {
                (req as any).jwt = verify(accessToken, ACCESS_TOKEN_SECRET) as any;
            } catch {
                (req as any).jwt = undefined;
            }
        }

        next();
    }

}
