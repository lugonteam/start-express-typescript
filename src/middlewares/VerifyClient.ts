import express from 'express';
import {ExpressMiddlewareInterface, Middleware} from "express-lugon";
import {deFull} from "../utils/encode/CryptoUtil";
import {env} from "../env";
import {$log} from "ts-log-debug";

@Middleware({type: 'before'})
export class VerifyClient implements ExpressMiddlewareInterface {

    public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
        if (env.isProduction && env.app.verifyClient) {
            const {csrf, force} = req.headers
            const {force: forceQuery} = req.query
            const userAgentReq = req.headers['user-agent']
            if (!!force || !!forceQuery) {
                $log.warn("Force next middleware")
                next();
            } else {

                let isError = false
                try {
                    const {userAgent, time} = JSON.parse(deFull(csrf))
                    console.log({userAgent, time})
                    if (
                        userAgent && time
                        && userAgent === userAgentReq
                        && (time >= Date.now() - 60 * 5 * 1000)
                    ) {
                        next()
                    } else {
                        isError = true
                    }
                } catch (e) {
                    console.log(e)
                    throw Error("Refresh browser and try again.")
                }
                if (isError)
                    throw Error("Refresh browser and try again.")
            }
        } else {
            next()
        }
    }

}
