import "reflect-metadata";
import {Action, ServerLoader, ServerSettings, useContainer} from "express-lugon";
import {Container, Service} from "typedi";
import {$log} from "ts-log-debug";
import path from "path";
import cookieParser from "cookie-parser";
import {iocLoader} from "./loaders/iocLoader";
import {winstonLoader} from "./loaders/winstonLoader";
import {publicLoader} from "./loaders/publicLoader";
import {homeLoader} from "./loaders/homeLoader";
import {banner} from "./lib/banner";
import {env} from "./env";
import {GetCurrentUser} from "./auth/authorizationChecker";
import {eventDispatchLoader} from "./loaders/eventDispatchLoader";
import Http from "http";
import {typeOrmLoader} from "./loaders/typeOrmLoader";

useContainer(Container);
export const rootDir = __dirname;
export const rootPath = () => {
    if (rootDir.endsWith("/dist")) {
        return path.resolve(rootDir, "..");
    } else {
        return rootDir;
    }
};

@Service()
@ServerSettings({
    project: "Routing Controllers",
    port: <string>env.app.port,
    rootDir,
    required: [
        rootDir + "/logger",
        rootDir + "/repository",
        // rootDir + "/modules/dc",
    ],
    loaders: [
        winstonLoader,
        iocLoader,
        eventDispatchLoader,
        homeLoader,
        publicLoader,
    ],
    options: {
        defaults: {
            nullResultCode: 404,
            undefinedResultCode: 520,
            paramOptions: {
                required: true,
            }
        },
        cors: true,
        defaultErrorHandler: false,
        controllers:
            [
                // rootDir + "/controllersTest/*{.js,.ts}",
                rootDir + "/controllers/*{.js,.ts}",
                // rootDir + "/controllers/admin/*{.js,.ts}",
                // rootDir + "/controllers/facebookController/*{.js,.ts}",
            ],
        middlewares: [
            rootDir + "/middlewares/global/{.js,.ts}",
        ],
        interceptors: [],
        authorizationChecker: async (action: Action, roles: string[]) => {
            const user = GetCurrentUser(action.request);
            return !!user;
        },
        currentUserChecker: (action: Action) => {
            return GetCurrentUser(action.request)
        }
    }
})

export class Server extends ServerLoader {

    // $beforeInit(): Promise<any> {
    //     return this.runLoaders([typeOrmLoader])
    //         .then(() => {
    //             $log.info("Setting is loaded.");
    //         })
    //         .catch(console.error)
    // }

    $onInit(): void {
        $log.info("ROOT path: " + rootPath());
        $log.info("PROJECT: " + this.project);

        const morgan = require('morgan'),
            bodyParser = require('body-parser');
        this
            .use(morgan('dev'))
            .use(cookieParser())
            .use(bodyParser.json({limit: '50mb', extended: true}))
            .use(bodyParser.raw({limit: '50mb', extended: true}))
            .use(bodyParser.urlencoded({limit: '50mb', extended: true}))
            .loaders(true)
            .then(_ => {
                $log.info("Loaders are loaded.");
            })
    }

    async $subscriptionServer(httpServer: Http.Server) {
    }

    $onCreatedServer(httpServer: Http.Server) {
    }

    $onReady() {
        $log.debug('Server initialized');
        $log.debug('PORT: ' + this.settings.port);
    }

    $onServerInitError(error: any): any {
        console.log(error)
        $log.error('Server encounter an error =>', JSON.stringify(error));
    }
}

new Server()
    .start()
    .then(() => {
        $log.info('Server started...');
        banner($log);
    })
    .catch((err) => {
        $log.error(err);
    });
