import dotenv from 'dotenv';
import {join} from 'path';
import {getOsEnv, getOsEnvOptional, getOsPath, getOsPaths, normalizePort, toBool, toNumber} from './lib/env';
import {$log} from "ts-log-debug";

/**
 * Load .env file or for tests the .env.test file.
 */
const pathEnv = join(process.cwd(), `.env${((process.env.NODE_ENV === 'test') ? '.test' : '')}`);
console.log({pathEnv});
dotenv.config({path: pathEnv});
$log.info("environment: ", process.env.NODE_ENV);

const pkg: any = require('../package.json');
/**
 * Environment variables
 */
export const env = {
    project: pkg.name,
    node: process.env.NODE_ENV || 'development',
    isProduction: process.env.NODE_ENV && process.env.NODE_ENV.startsWith("prod"),
    isTest: process.env.NODE_ENV === 'test',
    isDevelopment: process.env.NODE_ENV.startsWith("dev"),
    app: {
        name: getOsEnv('APP_NAME', "StoxPixel"),
        version: pkg.version,
        description: pkg.description,
        host: getOsEnv('APP_HOST', "localhost"),
        schema: getOsEnv('APP_SCHEMA', "http"),
        routePrefix: getOsEnv('APP_ROUTE_PREFIX', "/api"),
        port: normalizePort(getOsEnv('APP_PORT', "4002")),
        banner: toBool(getOsEnv('APP_BANNER', "true")),
        verifyClient: toBool(getOsEnv('APP_VERIFY_CLIENT', "false")),
        dirs: {
            migrations: getOsPaths('TYPEORM_MIGRATIONS', "src/database/migrations/**/*.ts"),
            migrationsDir: getOsPath('TYPEORM_MIGRATIONS_DIR', "src/database/migrations"),
            entities: getOsPaths('TYPEORM_ENTITIES', "src/graphql/entities/**/*.ts"),
            entitiesDir: getOsPath('TYPEORM_ENTITIES_DIR', "src/graphql/entities"),
            controllers: getOsPaths('CONTROLLERS', "src/controllers/**/*Controller.ts"),
            middlewares: getOsPaths('MIDDLEWARES', "src/middlewares/**/*Middleware.ts"),
            interceptors: getOsPaths('INTERCEPTORS', "src/interceptors/**/*Interceptor.ts"),
            subscribers: getOsPaths('SUBSCRIBERS', "src/subscribers/**/*Subscriber.ts"),
            resolvers: getOsPaths('RESOLVERS', "src/graphql/resolvers/**/*.ts"),
        },
    },
    log: {
        level: getOsEnv('LOG_LEVEL') || "debug",
        json: toBool(getOsEnvOptional('LOG_JSON')),
        output: getOsEnv('LOG_OUTPUT') || "dev",
    },
    db: {
        type: getOsEnv('TYPEORM_CONNECTION') || "mongodb",
        host: getOsEnvOptional('TYPEORM_HOST'),
        url: getOsEnvOptional('TYPEORM_URL'),
        port: toNumber(getOsEnvOptional('TYPEORM_PORT')),
        username: getOsEnvOptional('TYPEORM_USERNAME'),
        password: getOsEnvOptional('TYPEORM_PASSWORD'),
        database: getOsEnv('TYPEORM_DATABASE'),
        synchronize: toBool(getOsEnvOptional('TYPEORM_SYNCHRONIZE')),
        logging: getOsEnv('TYPEORM_LOGGING'),
    },
    graphql: {
        enabled: toBool(getOsEnv('GRAPHQL_ENABLED', "true")),
        route: getOsEnv('GRAPHQL_ROUTE', "/graphql"),
        editor: toBool(getOsEnv('GRAPHQL_EDITOR', "true")),
        rateLimit: toNumber(getOsEnv('GRAPHQL_DEPTH_LIMIT', "5")),
    }
};
