import {BaseLayout, LogEvent, Layout} from "ts-log-debug";

@Layout({name: "log-json"})
export class JsonLayout extends BaseLayout {
    transform(loggingEvent: LogEvent, timezoneOffset?: number): string {
        const log = {
            time: loggingEvent.startTime,
            category: loggingEvent.categoryName,
            level: loggingEvent.level.toString(),
            data: loggingEvent.data,
        };

        return JSON.stringify(log) + (this.config["separator"] || "");
    };
}


