import Express from "express";
import {verify} from "jsonwebtoken";
import {ACCESS_TOKEN_SECRET} from "./config";

export const TYPE_AUTHORIZATION = ["Bearer", "Authorization", "Token", "JWT"];

export const AccessTokenDetection = function (request: Express.Request | any): string {
    try {
        const authorizationHeader = request.headers["" +
        "authorization"];
        const accessTokenQuery = !!request.query ? request.query.access_token || "" : "";
        const accessTokenBody = !!request.body ? request.body.access_token || "" : "";
        const accessTokenHeader = (!authorizationHeader || TYPE_AUTHORIZATION.indexOf(authorizationHeader.split(" ")[0]) === -1)
            ? "" : authorizationHeader.split(" ")[1];
        return accessTokenHeader ? accessTokenHeader : accessTokenBody ? accessTokenBody : accessTokenQuery;
    } catch (e) {
        console.log({AccessTokenDetection: e});
        return "";
    }
};

export const GetCurrentUser = (request: Express.Request) => {
    const accessToken = AccessTokenDetection(request);
    if (accessToken) {
        try {
            return verify(accessToken, ACCESS_TOKEN_SECRET) as any;
        } catch {
            return undefined;
        }
    }
    return undefined
};
