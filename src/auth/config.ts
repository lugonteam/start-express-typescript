export const ACCESS_TOKEN_SECRET = "3DID29l0Reui61xpkWhMEGj4hvjv7657fhf";
// export const ACCESS_TOKEN_COOKIE_EXPIRY_SECONDS = 24 * 60 * 60; // 1days
export const ACCESS_TOKEN_COOKIE_EXPIRY_SECONDS = 2 * 60 * 60; // 2hours
// export const ACCESS_TOKEN_COOKIE_EXPIRY_SECONDS = 30; // 30s
export const ACCESS_TOKEN_COOKIE_HTTPS = false;
export const IS_CSRF_CHECK_ENABLED = true;
