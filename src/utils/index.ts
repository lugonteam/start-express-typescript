import path from "path";

export function flattenObject(obj) {
    let toReturn = {};
    for (let i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if ((typeof obj[i]) == 'object' && obj[i] !== null) {
            let flatObject = flattenObject(obj[i]);
            for (let x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) continue;
                toReturn[i + '.' + x] = flatObject[x];
            }
        } else {
            toReturn[i] = obj[i];
        }
    }
    return toReturn;
}

export function fileNameByPath(filePath: string) {
    return filePath.split(path.sep).slice(-1)[0].split(".")[0]
}

export async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
}

export function waitFor(ms) {
    return new Promise(r => setTimeout(r, ms));
}

export function toObject(any: any) {
    return JSON.parse(JSON.stringify(any));
}

export function mergeObjectFilter<T>(obj1: T, obj2: T, keys: Array<string>, isCheckNull: boolean = false) {
    let newObj = {};
    let keysObj = Object.keys(obj1);
    keys.filter(e => keysObj.indexOf(e) !== -1).forEach(key => {
        if (isCheckNull) {
            newObj[key] = (obj2[key] !== null && obj2[key] !== undefined) ? obj2[key] : obj1[key] || null
        } else {
            newObj[key] = obj2[key] || obj1[key] || null
        }
    });
    return newObj;
}

export function chooseObjectFilter<T>(obj: T, keys: string[]) {
    let newObj = {};
    let keysObj = Object.keys(obj);
    keys.filter(key => keysObj.indexOf(key) !== -1).forEach(key => {
        newObj[key] = obj[key] || ""
    });
    return newObj;
}

export function duplicateElements(arr: any[], fieldCompare: string = "") {
    const compareArrayTemp: any[] = [];
    const results: any[] = [];
    arr.forEach((ele: any) => {
        if (!!fieldCompare) {
            if (compareArrayTemp.indexOf(ele[fieldCompare]) !== -1) {
                results.push(ele);
            } else {
                compareArrayTemp.push(ele[fieldCompare]);
            }
        } else {
            if (compareArrayTemp.indexOf(ele) !== -1) {
                results.push(ele);
            } else {
                compareArrayTemp.push(ele);
            }
        }
    });
    return results;
}

export function roundUp(num: number, precision: number = 8) {
    precision = Math.pow(10, precision);
    return Math.ceil(num * precision) / precision
}

export function sumArrayNumber(arr: number[]) {
    return arr.reduce(function (acc, val) {
        return acc + val;
    }, 0)
}

export function toPlainStringSmallNumber(num: number) {
    return ('' + +num).replace(/(-?)(\d*)\.?(\d*)e([+-]\d+)/,
        function (a, b, c, d, e) {
            return e < 0
                ? b + '0.' + Array(1 - e - c.length).join('0') + c + d
                : b + c + d + Array(e - d.length + 1).join('0');
        });
}

export function chunkArray(input: any[], perChunk: number) {
    return input.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / perChunk)

        if (!resultArray[chunkIndex]) {
            resultArray[chunkIndex] = [] // start a new chunk
        }

        resultArray[chunkIndex].push(item)

        return resultArray
    }, [])
}
