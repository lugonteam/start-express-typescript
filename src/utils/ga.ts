import ua from 'universal-analytics'
import {env} from "../env";
import {Service} from "typedi";
import {$log} from "ts-log-debug";

export interface IGaEvent {
    category: string,
    action: string,
    label?: string
    value?: number
}

@Service()
export class Ga {
    private readonly gaId: string;
    private readonly visitor: any;

    constructor() {
        this.gaId = process.env.GA_ID || "UA-177079906-3";
        this.visitor = ua(this.gaId, {http: true});
    }

    event(ev: IGaEvent) {
        try {
            this.visitor.event(ev.category, ev.action, env.isDevelopment ? "env dev" : ev.label, ev.value).send()
        } catch (e) {
            $log.error(e.message)
        }
    }
}
