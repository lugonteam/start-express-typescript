export interface IFileUpload {
    filename: string
    fieldname: string
    path: string
    originalname: string
    encoding: string
    mimetype: string
    destination: string
    size: number
}

export function handlerPathFileUpload(file: IFileUpload, destinationOriginal: string) {
    const {destination, path} = file;
    file.destination = destinationOriginal + destination.split(destinationOriginal)[1];
    file.path = destinationOriginal + path.split(destinationOriginal)[1];
    return file
}