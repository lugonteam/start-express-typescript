import {enFull, deFull} from "./CryptoUtil"

export const Crypto = {
    encrypt: enFull,
    decrypt: deFull,
};