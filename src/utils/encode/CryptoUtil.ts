const CryptoJS = require("crypto-js");
const crypto = require('crypto');
const keyBase64 = "DWIzFkO22qfVMgx2fIsxOXnwz10pRuZfFJBvf4RS3eY=";
const ivBase64 = 'AcynMwikMkW4c7+mHtwtfw==';

function encrypt(decrypted, keyBase64, ivBase64) {
    const key = Buffer.from(keyBase64, 'base64');
    const iv = Buffer.from(ivBase64, 'base64');
    const cipher = crypto.createCipheriv(getAlgorithm(keyBase64), key, iv);
    let encrypted = cipher.update(decrypted, 'utf8', 'base64');
    encrypted += cipher.final('base64');
    return encrypted;
}

function decrypt(encrypted, keyBase64, ivBase64) {
    const key = Buffer.from(keyBase64, 'base64');
    const iv = Buffer.from(ivBase64, 'base64');
    const decipher = crypto.createDecipheriv(getAlgorithm(keyBase64), key, iv);
    let decrypted = decipher.update(encrypted, 'base64');
    decrypted += decipher.final();
    return decrypted;
}

function getAlgorithm(keyBase64) {
    const key = Buffer.from(keyBase64, 'base64');
    switch (key.length) {
        case 16:
            return 'aes-128-cbc';
        case 32:
            return 'aes-256-cbc';
    }
    throw new Error('Invalid key length: ' + key.length);
}

export function quickEncrypt(decrypted) {
    return encrypt(decrypted, keyBase64, ivBase64);
}

export function quickDecrypt(encrypted) {
    return decrypt(encrypted, keyBase64, ivBase64);
}

export function enFull(decrypted) {
    return encode64(quickEncrypt(ul(encode64(ul(decrypted)))));
}

export function deFull(encrypted) {
    return ul(decode64(ul(quickDecrypt(decode64(encrypted)))))
}

export function encode64(text: string): string {
    return Buffer.from(text).toString('base64')
}

export function decode64(code: string): string {
    return Buffer.from(code, 'base64').toString('ascii')
}

export function ul(text: string): string {
    return text.split("")
        .map(it => {
            if (isUpperCase(it)) return it.toLowerCase();
            else return it.toUpperCase();
        })
        .join("")
}

export function aesEncrypt(text: string, secret: string) {
    return CryptoJS.AES.encrypt(text, secret).toString();
}

export function aesDecrypt(text: string, secret) {
    const bytes = CryptoJS.AES.decrypt(text, secret);
    return bytes.toString(CryptoJS.enc.Utf8);
}

export function enSecret(text: string, secret: string): string {
    return aesEncrypt(ul(encode64(ul(text))), secret)
}

export function deSecret(text: string, secret: string): string {
    return ul(decode64(ul(aesDecrypt(text, secret))))
}

function isUpperCase(it: string): boolean {
    return it === it.toUpperCase()
}
